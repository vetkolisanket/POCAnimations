package vetkoli.sanket.pocanimations;

import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    private ImageButton imageButton;

    private boolean buttonState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageButton = (ImageButton) findViewById(R.id.imageButton);

        buttonState = false;

        imageButton.setImageDrawable(getDrawable(R.drawable.vectalign_animated_vector_drawable_start_to_end));

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonState) {
                    imageButton.setImageDrawable(getDrawable(R.drawable.vectalign_animated_vector_drawable_end_to_start));
                    buttonState = !buttonState;
                    Animatable animatable = (Animatable) imageButton.getDrawable();
                    animatable.start();
                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imageButton.setImageDrawable(getDrawable(R.drawable.vectalign_animated_vector_drawable_start_to_end));
                            buttonState = !buttonState;
                        }
                    }, 2000);*/
                } else {
                    imageButton.setImageDrawable(getDrawable(R.drawable.vectalign_animated_vector_drawable_start_to_end));
                    buttonState = !buttonState;
                    Animatable animatable = (Animatable) imageButton.getDrawable();
                    animatable.start();
                    /*new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            imageButton.setImageDrawable(getDrawable(R.drawable.vectalign_animated_vector_drawable_end_to_start));
                            buttonState = !buttonState;
                        }
                    }, 2000);*/
                }
            }
        });
    }
}
